
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include <unistd.h>

#include <psl1ght/lv2/net.h>

#include <lv1_map.h>
#include <udp_printf.h>

#include <otheros_bootloader_loader.bin.h>

/*
 * GameOS system manager object in HV process 9
 */
#define GAMEOS_SYSMGR_OFFSET				0x000a5dd0ull

/*
 * log2 of boot memory size for GameOS (3.41)
 */
#define LOG2_PAGE_SIZE_OFFSET				(GAMEOS_SYSMGR_OFFSET + 0x1b0ull)
#define LOG2_PAGE_SIZE					0x0000001bull

/*
 * Ability mask for GameOS (3.41)
 */
#define ABILITY_MASK_OFFSET				(GAMEOS_SYSMGR_OFFSET + 0x1c0ull)
#define IOS_NET_EURUS_LPAR_BIT_MASK			(1ull << 9)
#define RM_IOS_NET_EURUS_LPAR_BIT_BRANCH_OFFSET		(0x150000ull + 0xa44ull)

/*
 * sll_load_lv2 (3.41)
 */
#define SLL_LOAD_LV2_OFFSET	0x00165e44ull

/*
 * main
 */
int main(int argc, char **argv)
{
	uint64_t val64;
	uint32_t val32;
	int result;

	netInitialize();

	udp_printf_init();

	PRINTF("%s:%d: start\n", __func__, __LINE__);

	PRINTF("%s:%d: OtherOS bootloader loader size (0x%08x)\n",
		__func__, __LINE__, sizeof(otheros_bootloader_loader_bin));

	result = lv1_map();
	if (result) {
		PRINTF("%s:%d: lv1_map failed (0x%08x)\n", __func__, __LINE__, result);
		goto done;
	}

	PRINTF("%s:%d: patching log2 page size\n", __func__, __LINE__);

	lv1_poke(LOG2_PAGE_SIZE_OFFSET, LOG2_PAGE_SIZE);

	/*
	 * Patch ios.net.eurus.lpar bit in ability mask because GameOS uses HV calls 195 and 196
	 * to access PS3 gelic wireless device but Linux uses HV call lv1_net_control !!!
	 */

	PRINTF("%s:%d: patching ios.net.eurus.lpar bit in ability mask\n", __func__, __LINE__);

	val64 = lv1_peek(ABILITY_MASK_OFFSET);
	val64 &= ~IOS_NET_EURUS_LPAR_BIT_MASK;
	lv1_poke(ABILITY_MASK_OFFSET, val64);

	val32 = 0x60000000ul; /* nop opcode */
	lv1_memcpy_to(RM_IOS_NET_EURUS_LPAR_BIT_BRANCH_OFFSET, &val32, sizeof(val32));

	PRINTF("%s:%d: patching sll load lv2\n", __func__, __LINE__);

	lv1_memcpy_to(SLL_LOAD_LV2_OFFSET, otheros_bootloader_loader_bin, sizeof(otheros_bootloader_loader_bin));

	PRINTF("%s:%d: end\n", __func__, __LINE__);

done:

	result = lv1_unmap();
	if (result)
		PRINTF("%s:%d: lv1_unmap failed (0x%08x)\n", __func__, __LINE__, result);

	udp_printf_deinit();

	netDeinitialize();

	return 0;
}
